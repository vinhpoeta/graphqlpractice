﻿using GraphQLPractice.Core.DomModel;
using GraphQLPractice.Core.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using GenFu;
using System.Linq;
using GraphQLPractice.Core.Model;

namespace GraphQLPractice.Data.Repository
{
    public class PeopleRepository : IPeopleDataSource
    {
        private ApplicationDbContext _dbContext;
        private IModelResolver _modResolver;

        public PeopleRepository(ApplicationDbContext dbContext, IModelResolver modResolver)
        {
            _dbContext = dbContext;
            _modResolver = modResolver;
        }

        public IEnumerable<Person> GetAllPeople()
        {
            throw new NotImplementedException();
        }

        public Person GetPerson(object id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Person> GetPersonByName(string name)
        {
            throw new NotImplementedException();
        }
    }
}
