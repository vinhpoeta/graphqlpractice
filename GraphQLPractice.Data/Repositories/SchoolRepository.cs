﻿using GraphQLPractice.Core.DomModel;
using GraphQLPractice.Core.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using GenFu;
using System.Linq;
using GraphQLPractice.Core.Model;

namespace GraphQLPractice.Data.Repository
{
    public class SchoolRepository : ISchoolDataSource, IClassDataSource, IStudentDataSource, ITeacherDataSource
    {
        private ApplicationDbContext _dbContext;
        private IModelResolver _modResolver;

        public SchoolRepository(ApplicationDbContext dbContext, IModelResolver modResolver)
        {
            _dbContext = dbContext;
            _modResolver = modResolver;
        }

        public IEnumerable<School> GetAllSchools()
        {
            var allSchools = new List<School>();

            var allSchoolEnts = _dbContext.Schools.ToList();
            if (allSchoolEnts != null && allSchoolEnts.Count > 0)
            {
                foreach (var schoolEnt in allSchoolEnts)
                {
                    var school = _modResolver.Resolve<School>();
                    if (school != null)
                    {
                        school.Id = schoolEnt.Id;
                        school.Name = schoolEnt.Name;
                        school.Address = schoolEnt.Address;
                    }
                }
            }

            return allSchools;
        }

        public School GetSchool(object id)
        {
            var schoolEnt = _dbContext.Schools.Find(id);

            if (schoolEnt == null)
            {
                return null;
            }

            var school = _modResolver.Resolve<School>();
            if (school != null)
            {
                school.Id = schoolEnt.Id;
                school.Name = schoolEnt.Name;
                school.Address = schoolEnt.Address;
            }

            return school;
        }

        public School GetSchoolOfClass(object id)
        {
            var classEnt = _dbContext.Classes.Find(id);
            if (classEnt != null)
            {
                var school = _modResolver.Resolve<School>();
                if (school != null)
                {
                    school.Id = classEnt.School.Id;
                    school.Name = classEnt.School.Name;
                    school.Address = classEnt.School.Address;

                    return school;
                }
            }

            return null;
        }

        public IEnumerable<Class> GetAllClasses(object schoolId)
        {
            var selectedClasses = new List<Class>();

            var schoolIdVal = -1;
            if (int.TryParse(schoolId.ToString(), out schoolIdVal))
            {
                var classEnts = _dbContext.Classes.Where(ce => ce.SchoolId == schoolIdVal).ToList();
                if (classEnts != null && classEnts.Count > 0)
                {
                    foreach (var classEnt in classEnts)
                    {
                        var selectedClass = _modResolver.Resolve<Class>();
                        selectedClass.Id = classEnt.Id;
                        selectedClass.Name = classEnt.ClassName;
                        selectedClass.Code = classEnt.ClassCode;

                        selectedClasses.Add(selectedClass);
                    }
                }
            }

            return selectedClasses;
        }

        public IEnumerable<Class> GetAllClassesOfStudent(object studentId)
        {
            var selectedClasses = new List<Class>();

            var stuIdVal = -1;
            if (int.TryParse(studentId.ToString(), out stuIdVal))
            {
                // Looking for all class participations of the student
                var classPartEnts = _dbContext.ClassParticipations.Where(cpe => cpe.StudentId == stuIdVal).ToList();
                if (classPartEnts != null && classPartEnts.Count > 0)
                {
                    foreach (var classPartEnt in classPartEnts)
                    {
                        var classEnt = _dbContext.Classes.Find(classPartEnt.ClassId);
                        if (classEnt != null)
                        {
                            var selectedClass = _modResolver.Resolve<Class>();
                            selectedClass.Id = classEnt.Id;
                            selectedClass.Name = classEnt.ClassName;
                            selectedClass.Code = classEnt.ClassCode;

                            selectedClasses.Add(selectedClass);
                        }
                    }
                }
            }

            return selectedClasses;
        }

        public IEnumerable<Class> GetAllClassesOfTeacher(object teacherId)
        {
            var selectedClasses = new List<Class>();

            var tchIdVal = -1;
            if (int.TryParse(teacherId.ToString(), out tchIdVal))
            {
                // Looking for all class participations of the student
                var classAssEnts = _dbContext.ClassAssignments.Where(cae => cae.TeacherId == tchIdVal).ToList();
                if (classAssEnts != null && classAssEnts.Count > 0)
                {
                    foreach (var classAssEnt in classAssEnts)
                    {
                        var classEnt = _dbContext.Classes.Find(classAssEnt.ClassId);
                        if (classEnt != null)
                        {
                            var selectedClass = _modResolver.Resolve<Class>();
                            selectedClass.Id = classEnt.Id;
                            selectedClass.Name = classEnt.ClassName;
                            selectedClass.Code = classEnt.ClassCode;

                            selectedClasses.Add(selectedClass);
                        }
                    }
                }
            }

            return selectedClasses;
        }

        public Class GetClass(object id)
        {
            var classEnt = _dbContext.Classes.Find(id);
            if (classEnt != null)
            {
                var @class = _modResolver.Resolve<Class>();
                @class.Id = classEnt.Id;
                @class.Name = classEnt.ClassName;
                @class.Code = classEnt.ClassCode;

                return @class;
            }

            return null;
        }

        public IEnumerable<Student> GetAllStudents(object schoolId)
        {
            throw new NotImplementedException();
        }

        public Student GetStudent(object id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Student> GetPrimaryStudents(object schoolId)
        {
            return GetAllStudents(schoolId).Where(s => s.Age <= 10);
        }

        public IEnumerable<Student> GetSecondaryStudents(object schoolId)
        {
            return GetAllStudents(schoolId).Where(s => s.Age >= 11 && s.Age <= 14);
        }

        public IEnumerable<Student> GetHighSchoolStudents(object schoolId)
        {
            return GetAllStudents(schoolId).Where(s => s.Age >= 15);
        }

        public IEnumerable<Student> GetStudentsByClass(object classId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Teacher> GetAllTeachers(object schoolId)
        {
            throw new NotImplementedException();
        }

        public Teacher GetTeacher(object id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Teacher> GetTeachersByClass(object classId)
        {
            throw new NotImplementedException();
        }
    }
}
