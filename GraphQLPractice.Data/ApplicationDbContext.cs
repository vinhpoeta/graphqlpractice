﻿using GraphQLPractice.Data.Entity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace GraphQLPractice.Data
{
    public class ApplicationDbContext : IdentityDbContext<AppUser>
    {
        public DbSet<School> Schools { get; set; }
        public DbSet<Class> Classes { get; set; }

        public DbSet<Person> People { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Teacher> Teachers { get; set; }

        public DbSet<ClassParticipation> ClassParticipations { get; set; }
        public DbSet<ClassAssignment> ClassAssignments { get; set; }

        public ApplicationDbContext(DbContextOptions options)
        : base(options)
        {
        }
    }
}
