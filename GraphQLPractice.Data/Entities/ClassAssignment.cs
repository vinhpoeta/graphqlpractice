﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GraphQLPractice.Data.Entity
{
    public class ClassAssignment : BaseDbEntity
    {
        public int TeacherId { get; set; }
        public Teacher Teacher { get; set; }

        public int ClassId { get; set; }
        public Class Class { get; set; }

        public DateTime? AssignedFrom { get; set; }
        public DateTime? AssignedTo { get; set; }
    }
}
