﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GraphQLPractice.Data.Entity
{
    public class School : BaseDbEntity
    {
        public string Name { get; set; }
        public string Address { get; set; }
        
        public List<Class> Classes { get; set; }
    }
}
