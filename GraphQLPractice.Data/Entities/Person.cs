﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GraphQLPractice.Data.Entity
{
    public class Person : BaseDbEntity
    {
        public string IdentityId { get; set; }
        public AppUser Identity { get; set; }

        public string Name { get; set; }
        public int Age { get; set; }

        public GenderType? Gender { get; set; }
    }

    public enum GenderType
    {
        Male, Female
    }
}
