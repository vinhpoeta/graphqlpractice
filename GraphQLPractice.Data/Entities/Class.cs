﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GraphQLPractice.Data.Entity
{
    public class Class : BaseDbEntity
    {
        public string ClassCode { get; set; }
        public string ClassName { get; set; }

        public int SchoolId { get; set; }
        public School School { get; set; }
    }
}
