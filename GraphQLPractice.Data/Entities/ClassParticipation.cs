﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GraphQLPractice.Data.Entity
{
    public class ClassParticipation : BaseDbEntity
    {
        public int StudentId { get; set; }
        public Student Student { get; set; }

        public int ClassId { get; set; }
        public Class Class { get; set; }

        public DateTime? ParticipateFrom { get; set; }
        public DateTime? ParticipateTo { get; set; }
    }
}
