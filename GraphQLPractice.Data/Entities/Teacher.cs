﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GraphQLPractice.Data.Entity
{
    public class Teacher : Person
    {
        public DateTime? TeachFrom { get; set; }
    }
}
