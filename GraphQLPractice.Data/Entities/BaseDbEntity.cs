﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GraphQLPractice.Data.Entity
{
    public class BaseDbEntity
    {
        public int Id { get; set; }
    }
}
