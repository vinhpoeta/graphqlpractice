﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GraphQLPractice.Data.Entity
{
    public class Student : Person
    {
        public string StudentCode { get; set; }
    }
}
