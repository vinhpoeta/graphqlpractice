﻿using GraphQLPractice.Core.DomModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace GraphQLPractice.Core.Interface
{
    public interface IClassDataSource
    {
        IEnumerable<Class> GetAllClasses(object schoolId);
        IEnumerable<Class> GetAllClassesOfStudent(object studentId);
        IEnumerable<Class> GetAllClassesOfTeacher(object teacherId);
        Class GetClass(object id);
    }
}
