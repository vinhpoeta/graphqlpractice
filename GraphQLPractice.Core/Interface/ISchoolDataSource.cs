﻿using GraphQLPractice.Core.DomModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace GraphQLPractice.Core.Interface
{
    public interface ISchoolDataSource
    {
        IEnumerable<School> GetAllSchools();
        School GetSchool(object id);
        School GetSchoolOfClass(object classId);
    }
}
