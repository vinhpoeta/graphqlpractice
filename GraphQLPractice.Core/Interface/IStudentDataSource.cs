﻿using GraphQLPractice.Core.DomModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace GraphQLPractice.Core.Interface
{
    public interface IStudentDataSource
    {
        IEnumerable<Student> GetAllStudents(object schoolId);
        IEnumerable<Student> GetPrimaryStudents(object schoolId);
        IEnumerable<Student> GetSecondaryStudents(object schoolId);
        IEnumerable<Student> GetHighSchoolStudents(object schoolId);
        IEnumerable<Student> GetStudentsByClass(object classId);
        Student GetStudent(object id);
    }
}
