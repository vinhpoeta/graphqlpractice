﻿using GraphQLPractice.Core.DomModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace GraphQLPractice.Core.Interface
{
    public interface ITeacherDataSource
    {
        IEnumerable<Teacher> GetAllTeachers(object schoolId);
        IEnumerable<Teacher> GetTeachersByClass(object classId);
        Teacher GetTeacher(object id);
    }
}
