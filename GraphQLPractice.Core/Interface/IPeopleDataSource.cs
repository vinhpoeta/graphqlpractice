﻿using GraphQLPractice.Core.DomModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace GraphQLPractice.Core.Interface
{
    public interface IPeopleDataSource
    {
        IEnumerable<Person> GetAllPeople();
        Person GetPerson(object id);
        IEnumerable<Person> GetPersonByName(string name);
    }
}
