﻿using GraphQLPractice.Core.Interface;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Text;

namespace GraphQLPractice.Core.DomModel
{
    public class School : BaseDmModel
    {
        private IClassDataSource _dsClasses;

        private ObservableCollection<Class> _classes;

        public string Name { get; set; }
        public string Address { get; set; }
        public IList<Class> Classes
        {
            get
            {
                if (_classes == null)
                {
                    var listOfClasses = _dsClasses.GetAllClasses(this.Id);
                    if (listOfClasses == null)
                    {
                        _classes = new ObservableCollection<Class>(listOfClasses);
                        _classes.CollectionChanged += Classes_CollectionChanged;
                    }
                }

                return _classes;
            }
        }

        public School(IClassDataSource dsClasses)
        {
            _dsClasses = dsClasses;
        }

        private void Classes_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    // TODO: Handle adding classes
                    break;
                case NotifyCollectionChangedAction.Remove:
                    // TODO: Handle removing classes
                    break;
                default:
                    break;
            }
        }
    }
}
