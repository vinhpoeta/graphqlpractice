﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GraphQLPractice.Core.DomModel
{
    public abstract class Person : BaseDmModel
    {
        public string Name { get; set; }
        public int Age { get; set; }
    }
}
