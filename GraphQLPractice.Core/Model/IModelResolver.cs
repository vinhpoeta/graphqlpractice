﻿using GraphQLPractice.Core.DomModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace GraphQLPractice.Core.Model
{
    public interface IModelResolver
    {
        T Resolve<T>(string key = null) where T : BaseDmModel;
    }
}
