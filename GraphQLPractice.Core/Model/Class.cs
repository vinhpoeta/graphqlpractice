﻿using GraphQLPractice.Core.Interface;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;

namespace GraphQLPractice.Core.DomModel
{
    public class Class : BaseDmModel
    {
        private ISchoolDataSource _dsSchools;
        private IStudentDataSource _dsStudents;

        private School _school;
        private ObservableCollection<Student> _students;

        public string Name { get; set; }
        public string Code { get; set; }

        public School School
        {
            get
            {
                if (_school == null)
                {
                    _school = _dsSchools.GetSchoolOfClass(this.Id);
                }

                return _school;
            }
        }

        public IList<Student> Students
        {
            get
            {
                if (_students == null)
                {
                    var listOfStudents = _dsStudents.GetStudentsByClass(this.Id);
                    if (listOfStudents != null)
                    {
                        _students = new ObservableCollection<Student>(listOfStudents);
                        _students.CollectionChanged += Students_CollectionChanged;
                    }
                }

                return _students;
            }
        }

        public Class(ISchoolDataSource dsSchools, IStudentDataSource dsStudents)
        {
            _dsSchools = dsSchools;
            _dsStudents = dsStudents;
        }

        private void Students_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    // TODO: Handle adding students
                    break;
                case NotifyCollectionChangedAction.Remove:
                    // TODO: Handle removing students
                    break;
                default:
                    break;
            }
        }
    }
}
