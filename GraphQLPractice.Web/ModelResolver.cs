﻿using GraphQLPractice.Core.DomModel;
using GraphQLPractice.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQLPractice.Web
{
    public class ModelResolver : IModelResolver
    {
        private IServiceProvider _srvProvider;

        public ModelResolver(IServiceProvider srvProvider)
        {
            _srvProvider = srvProvider;
        }

        public T Resolve<T>(string key = null) where T : BaseDmModel
        {
            return (T)_srvProvider.GetService(typeof(T));
        }
    }
}
