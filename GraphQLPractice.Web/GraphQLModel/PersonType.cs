﻿using GraphQL.Types;
using GraphQLPractice.Core.DomModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQLPractice.Web.GraphQLModel
{
    public class PersonType : InterfaceGraphType<Person>
    {
        public PersonType()
        {
            Field(x => x.Id).Description("The Id of the person");
            Field(x => x.Name, nullable: true).Description("The name of the person");
            Field(x => x.Age, nullable: true).Description("The age of the person");
        }
    }
}
