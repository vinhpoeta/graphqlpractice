﻿using GraphQL.Types;
using GraphQLPractice.Core.DomModel;
using GraphQLPractice.Core.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQLPractice.Web.GraphQLModel
{
    public class ClassType : ObjectGraphType<Class>
    {
        public ClassType()
        {
            Field(x => x.Id).Description("The Id of the class");
            Field(x => x.Name, nullable: true).Description("The name of the class");
            Field<SchoolType>(
                name: "school",
                description: "The school that contains the class",
                resolve: context =>
                {
                    return context.Source.School;
                }
            );
        }
    }
}
