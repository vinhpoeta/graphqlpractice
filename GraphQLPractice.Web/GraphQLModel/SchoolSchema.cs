﻿using GraphQL;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQLPractice.Web.GraphQLModel
{
    public class SchoolSchema : Schema
    {
        public SchoolSchema()
        {
        }

        public SchoolSchema(Func<Type, IGraphType> resolveType) : base(resolveType)
        {
        }

        public SchoolSchema(IDependencyResolver dependencyResolver) : base(dependencyResolver)
        {
        }
    }
}
