﻿using GraphQL.Types;
using GraphQLPractice.Core.DomModel;
using GraphQLPractice.Core.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQLPractice.Web.GraphQLModel
{
    public class TeacherType : ObjectGraphType<Teacher>, IImplementInterfaces
    {
        public TeacherType()
        {
            Interfaces = new Type[] { typeof(PersonType) };
            Field(x => x.Id).Description("The Id of the teacher");
            Field(x => x.Name, nullable: true).Description("The name of the teacher");
            Field(x => x.Age, nullable: true).Description("The age of the teacher");
            Field(
                name: "teaching_classes",
                description: "Classes that the teacher teaches",
                type: typeof(ListGraphType<ClassType>),
                resolve: context => context.Source.Classes
            );
        }
    }
}
