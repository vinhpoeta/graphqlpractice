﻿using GraphQL.Types;
using GraphQLPractice.Core.DomModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQLPractice.Web.GraphQLModel
{
    public class StudentType : ObjectGraphType<Student>, IImplementInterfaces
    {
        public StudentType()
        {
            Interfaces = new Type[] { typeof(PersonType) };
            Field(x => x.Id).Description("The Id of the person");
            Field(x => x.Name, nullable: true).Description("The name of the person");
            Field(x => x.Age, nullable: true).Description("The age of the person");
            Field<ListGraphType<ClassType>>(
                name: "studying_classes",
                description: "The class that the student studies",
                resolve: context => context.Source.Classes
            );
        }

        public enum StudentLevel
        {
            PRIMARY,
            SECONDARY,
            HIGH_SCHOOL,
            ALL
        }
    }
}
