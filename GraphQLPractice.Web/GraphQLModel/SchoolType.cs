﻿using GraphQL.Types;
using GraphQLPractice.Core.DomModel;
using GraphQLPractice.Core.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQLPractice.Web.GraphQLModel
{
    public class SchoolType : ObjectGraphType<School>
    {
        public SchoolType()
        {
            Field(x => x.Id).Description("The Id of the school");
            Field(x => x.Name, nullable: true).Description("The name of the school");
            Field(x => x.Address, nullable: true).Description("The address of the school");
        }
    }
}
