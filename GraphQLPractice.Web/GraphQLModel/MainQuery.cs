﻿using GraphQL.Types;
using GraphQLPractice.Core.Interface;
using static GraphQLPractice.Web.GraphQLModel.StudentType;

namespace GraphQLPractice.Web.GraphQLModel
{
    public class MainQuery : ObjectGraphType
    {
        public MainQuery(
            IPeopleDataSource dsPeople,
            ISchoolDataSource dsSchools,
            IClassDataSource dsClasses,
            IStudentDataSource dsStudents,
            ITeacherDataSource dsTeachers)
        {
            /*
             * QUERY FOR PEOPLE
             * */

            Field<PersonType>(
                "person",
                arguments: new QueryArguments(new QueryArgument<IntGraphType>() { Name = "id" }),
                resolve: context =>
                {
                    var stuId = context.GetArgument("id", -1);

                    return dsStudents.GetStudent(stuId);
                });
            Field<ListGraphType<PersonType>>("people", resolve: context => dsPeople.GetAllPeople());


            /*
             * QUERY FOR SCHOOL
             * */

            Field<ListGraphType<SchoolType>>(
                "schools",
                resolve: context =>
                {
                    return dsSchools.GetAllSchools();
                }
            );

            Field<ListGraphType<ClassType>>(
                "classes",
                arguments: new QueryArguments(
                    new QueryArgument<IntGraphType>() { Name = "school_id" }
                ),
                resolve: context =>
                {
                    var schoolId = context.GetArgument("school_id", -1);

                    return dsClasses.GetAllClasses(schoolId);
                }
            );

            Field<ListGraphType<StudentType>>(
                "students",
                arguments: new QueryArguments(
                    new QueryArgument<IntGraphType>() { Name = "school_id" },
                    new QueryArgument<EnumerationGraphType<StudentLevel>>() { Name = "level" }
                ),
                resolve: context =>
                {
                    var schoolId = context.GetArgument("school_id", -1);
                    var stuLevel = context.GetArgument("level", StudentLevel.ALL);
                    switch (stuLevel)
                    {
                        case StudentLevel.PRIMARY:
                            return dsStudents.GetPrimaryStudents(schoolId);
                        case StudentLevel.SECONDARY:
                            return dsStudents.GetSecondaryStudents(schoolId);
                        case StudentLevel.HIGH_SCHOOL:
                            return dsStudents.GetHighSchoolStudents(schoolId);
                        default:
                            return dsStudents.GetAllStudents(schoolId);
                    }
                }
            );

            Field<ListGraphType<TeacherType>>(
                "teachers",
                arguments: new QueryArguments(
                    new QueryArgument<IntGraphType>() { Name = "school_id" }
                ),
                resolve: context =>
                {
                    var schoolId = context.GetArgument("school_id", -1);

                    return dsTeachers.GetAllTeachers(schoolId);
                }
            );
        }
    }
}
