﻿using AutoMapper;
using GraphQLPractice.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQLPractice.Web.Forms.Mapping
{
    public class FormToDomainEntityMappingProfile : Profile
    {
        public FormToDomainEntityMappingProfile()
        {
            CreateMap<RegistrationForm, AppUser>()
                .ForMember(
                    appUser => appUser.UserName,
                    config => config.MapFrom(form => form.Email)
                );
        }
    }
}
