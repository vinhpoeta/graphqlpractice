﻿using FluentValidation.Attributes;
using GraphQLPractice.Web.Forms.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQLPractice.Web.Forms
{
    [Validator(typeof(RegistrationFormValidator))]
    public class RegistrationForm
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Location { get; set; }
    }
}
