﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQLPractice.Web.Forms.Validation
{
    public class RegistrationFormValidator : AbstractValidator<RegistrationForm>
    {
        public RegistrationFormValidator()
        {
            RuleFor(form => form.Email).NotEmpty().WithMessage("Email cannot be empty");
            RuleFor(form => form.Password).NotEmpty().WithMessage("Password cannot be empty");
            RuleFor(form => form.Password).Length(6, 12).WithMessage("Password must be between 6 and 12 characters");
            RuleFor(form => form.FirstName).NotEmpty().WithMessage("FirstName cannot be empty");
            RuleFor(form => form.LastName).NotEmpty().WithMessage("LastName cannot be empty");
            RuleFor(form => form.Location).NotEmpty().WithMessage("Location cannot be empty");
        }
    }
}
