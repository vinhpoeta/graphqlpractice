﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQLPractice.Web.Forms.Validation
{
    public class LoginFormValidator : AbstractValidator<LoginForm>
    {
        public LoginFormValidator()
        {
            RuleFor(form => form.UserName).NotEmpty().WithMessage("Username cannot be empty");
            RuleFor(form => form.Password).NotEmpty().WithMessage("Password cannot be empty");
            RuleFor(form => form.Password).Length(6, 12).WithMessage("Password must be between 6 and 12 characters");
        }
    }
}
