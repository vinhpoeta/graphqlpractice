﻿using FluentValidation.Attributes;
using GraphQLPractice.Web.Forms.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQLPractice.Web.Forms
{
    [Validator(typeof(LoginFormValidator))]
    public class LoginForm
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
