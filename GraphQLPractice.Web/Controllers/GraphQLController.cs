﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using GraphQL;
using GraphQL.Types;
using GraphQLPractice.Core.Interface;
using GraphQLPractice.Web.GraphQLModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GraphQLPractice.Web.Controllers
{
    [Authorize(Policy = "ApiUser")]
    [Route("graphql")]
    public class GraphQLController : Controller
    {
        private IPeopleDataSource _dsPeople;
        private ISchoolDataSource _dsSchools;
        private IClassDataSource _dsClasses;
        private IStudentDataSource _dsStudents;
        private ITeacherDataSource _dsTeachers;

        public GraphQLController(
            IPeopleDataSource dsPeople,
            ISchoolDataSource dsSchools,
            IClassDataSource dsClasses,
            IStudentDataSource dsStudents,
            ITeacherDataSource dsTeachers)
        {
            _dsPeople = dsPeople;
            _dsSchools = dsSchools;
            _dsClasses = dsClasses;
            _dsStudents = dsStudents;
            _dsTeachers = dsTeachers;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] GraphQLQuery query)
        {
            var schema = new Schema { Query = new MainQuery(_dsPeople, _dsSchools, _dsClasses, _dsStudents, _dsTeachers) };

            var result = await new DocumentExecuter().ExecuteAsync(_ =>
            {
                _.Schema = schema;
                _.Query = query.Query;

            }).ConfigureAwait(false);

            if (result.Errors?.Count > 0)
            {
                foreach (var error in result.Errors)
                {
                    Console.WriteLine(error.Message);
                    Debug.WriteLine(error.Message);
                }

                return BadRequest();
            }

            return Ok(result);
        }
    }
}